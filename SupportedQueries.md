SELECT * FROM products WHERE store=2

title,brand,store,price,in_stock
Apple iPhone 5,2,2,599.99,true
Nokia Lumia 800,3,2,300,false
Samsung LED TV 51 inches,4,2,900,true

SELECT brand FROM products WHERE price > 600;
SELECT brand FROM products WHERE price > 900;
brand
2
2
4
4
4

SELECT MAX(price) FROM products;

MAX(price)
1099.99

SELECT UNIQ(store) FROM products WHERE in_stock=false;

UNIQ(store)
1
2
5
6
7

SELECT title FROM products WHERE in_stock=false AND brand=5;

title
L'Oreal Hair Conditioner
L'Oreal Hair Conditioner

SELECT title FROM products WHERE in_stock=false AND ( brand=5 OR store=2 ); 

title
Nokia Lumia 800
L'Oreal Hair Conditioner
L'Oreal Hair Conditioner
