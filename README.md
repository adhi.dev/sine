# sine
  sine is a select query engine to query csv files.

## Table of contents
* [Prerequisites](#prerequisites)
* [Binary download](#binary-download)
* [Usage](#usage)
* [License](#license)

## Prerequisites
  Java 7 or higher is required to run the jar file. Make sure you have the installed the latest version of Java.
  
## Binary download
  Download the jar file from [here](https://github.com/v-adhithyan/sine/releases/download/0.99/sine.jar).
  
## Usage
  * Open cmd(windows) or terminal(*nix) and cd to the directory containing sine jar.
  * Enter the follwing command to start sine console: `java -cp sine.jar me.adhithyan.sine.console.SineConsole`
  * Type `help;` in console to know how to use sine.
  * Before using select statements, kindly have a look at this [file](https://raw.githubusercontent.com/v-adhithyan/sine/master/SupportedQueries.md) to know about the list of supported select statements.
  
## License
  Released under MIT License.
