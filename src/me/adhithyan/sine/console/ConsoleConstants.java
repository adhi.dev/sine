package me.adhithyan.sine.console;

import java.util.LinkedList;

/**
*
* @author adhithyan-3592
*
*/

public class ConsoleConstants {
	public static final String PROMPT = "sine # ";
	public static final String WELCOME = "Welcome to sine {(s)elect query eng(ine) to query csv files}.\n"
			+ "Type 'help' to know how to use sine. \n"
			+ "Console environment is case insenstive.\n"
			+ "Type 'quit' or 'exit' to leave sine console.\n\n";
	public static final String EMPTY_STRING = "";
	public static final String QUIT = "quit";
	public static final String EXIT = "exit";
	public static final String NULL = "null";
	public static final String HELP = "help";
	
	public static final String SECONDS = " seconds.";
	public static final String MILLI_SECONDS = " milliseconds.";
	public static final String TIME_TAKEN = "Time taken:";

	public static final LinkedList<String> quit = new LinkedList<String>();
	
	static{
		quit.add("quit");
		quit.add("quit;");
		
		quit.add("exit");
		quit.add("exit;");
	}
}
