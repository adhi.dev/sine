package me.adhithyan.sine.console;

import java.util.Scanner;

import me.adhithyan.sine.core.ExecutionEngine;

/**
*
* @author adhithyan-3592
*
*/

public class SineConsole {
	public static void printElapsedTime(long start, long end){
		long elapsedTime = end - start;
	
		System.out.println(ConsoleConstants.EMPTY_STRING);
		if(elapsedTime < 1000){
			System.out.println(ConsoleConstants.TIME_TAKEN + elapsedTime + ConsoleConstants.MILLI_SECONDS);
		}else{
			double timeInSeconds =  (double)elapsedTime / 1000d;
			System.out.println(ConsoleConstants.TIME_TAKEN + timeInSeconds + ConsoleConstants.SECONDS);
		}
		System.out.println(ConsoleConstants.EMPTY_STRING);
		
	}
	
	public static void main(String[] args){
		String query = "";
		Scanner input = new Scanner(System.in);
		long start, end;
		
		System.out.println(ConsoleConstants.WELCOME);
		
		do{
			System.out.print(ConsoleConstants.PROMPT);
			query = input.nextLine().toLowerCase().trim();
			
			if(query.length() == 0 || query.equals(ConsoleConstants.EMPTY_STRING) || query == null || query.equals(ConsoleConstants.NULL) || query.contains(ConsoleConstants.EXIT) || query.contains(ConsoleConstants.QUIT)){
				continue;
			}
			
			start = System.currentTimeMillis();
			ExecutionEngine.execute(query);
			end = System.currentTimeMillis();
			
			if(!query.contains(ConsoleConstants.HELP)){
				printElapsedTime(start, end);
			}
		}while(!ConsoleConstants.quit.contains(query));
		
		input.close();
	}
}
