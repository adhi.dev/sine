//$Id$
package me.adhithyan.sine.core.exceptions;

/**
*
* @author adhithyan-3592
*
*/

public class QueryException extends Exception  {
	
	private static final long serialVersionUID = -6790038466891514654L;
	
	public QueryException(String message){
		super(message);
	}

}
