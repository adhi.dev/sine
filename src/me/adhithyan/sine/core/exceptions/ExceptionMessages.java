package me.adhithyan.sine.core.exceptions;

/**
*
* @author adhithyan-3592
*
*/

public class ExceptionMessages {
	public static String SEMICOLON_MISSING = "Malformed query: Semicolon is missing at the end of query.";
	public static String UNSUPPORTED_QUERY = "Unsupported query: Type 'help' to see the list of available queries.";
	
	public static String CSV_FILE_NOT_PROVIDED = "File name not provided. Please execute the query with file name. ";
	public static String NO_CSV_EXTENSION = "Please provide a file name with csv extension.";
	
	public static String FILE_NOT_LOADED = "Please load a file to use this command.";
	
	public static String UNRECOGNIZED_SELECT_QUERY = "Unable to recognize the select query.";
}
