package me.adhithyan.sine.core;

import me.adhithyan.sine.core.query.QueryType;

/**
*
* @author adhithyan-3592
*
*/

class QueryParser {

	public static void parse(String query){
		String queryType = query.split(" ")[0];
		QueryType.setQueryType(queryType);
	}
}
