package me.adhithyan.sine.core.query;

import me.adhithyan.csv.parser.CsvLoader;
import me.adhithyan.sine.core.exceptions.ExceptionMessages;
import me.adhithyan.sine.core.exceptions.QueryException;

/**
 *
 * @author adhithyan-3592
 *
 */

public class ShowQueryImpl implements Query {

	@Override
	public void execute(String query) throws QueryException {
		// TODO Auto-generated method stub
		if(CsvLoader.fileLoaded()){
			System.out.println(CsvLoader.getLoadedData());
		}else{
			throw new QueryException(ExceptionMessages.FILE_NOT_LOADED);
		}
	}

}
