package me.adhithyan.sine.core.query;

import java.util.Iterator;
import java.util.TreeMap;

/**
 *
 * @author adhithyan-3592
 *
 */

public class HelpQueryImpl implements Query {

	@Override
	public void execute(String query) {

		TreeMap<String, String> queryInfo = QueryList.getAvailableQueriesInfo();
		
		Iterator<String> it = queryInfo.keySet().iterator();
		StringBuilder help = new StringBuilder();

		while(it.hasNext()){
			String queryName = it.next();
			String queryFunction = queryInfo.get(queryName);
			
			help.append(queryName);
			help.append(queryFunction);
			help.append("\n");
		}
		
		System.out.println(help);
	}

}
