package me.adhithyan.sine.core.query;

/**
 *
 * @author adhithyan-3592
 *
 */

public class QueryConstants {
	public static final String MAX_PRICE_QUERY = "max_price_query";
	public static final String SELECT_BRAND_FROM_PRICE = "select_brand_from_price";
	public static final String WHERE = "where";
	public static final String UNIQ_STORE_QUERY = "uniq_store_query";
	public static final String STOCK_TITLE_BRAND = "stock_title_brand";
	public static final String STOCK_TITLE_BRAND_STORE = "stock_title_brand_store"; 
}
