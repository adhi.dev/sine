package me.adhithyan.sine.core.query;

/**
 *
 * @author adhithyan-3592
 *
 */

public class QueryType {
	public static String queryType = null;
	
	public static void setQueryType(String type){
		queryType = type;
	}
	
	public static String getQueryType(){
		return queryType;
	}
}
