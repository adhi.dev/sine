package me.adhithyan.sine.core.query;

/**
 *
 * @author adhithyan-3592
 *
 */

public interface Query {
	public void execute(String query) throws Exception; 
}
