package me.adhithyan.sine.core.query;

import me.adhithyan.csv.parser.CsvLoader;
import me.adhithyan.csv.parser.ParsedData;
import me.adhithyan.sine.core.exceptions.ExceptionMessages;
import me.adhithyan.sine.core.exceptions.QueryException;

/**
 *
 * @author adhithyan-3592
 *
 */

public class SelectQueryImpl implements Query {

	@Override
	public void execute(String query) throws QueryException {
		// TODO Auto-generated method stub
		
		if(!CsvLoader.fileLoaded()){
			throw new QueryException(ExceptionMessages.FILE_NOT_LOADED);
		}
		
		if(SelectQueryTypes.isValidSelectQuery(query)){
			System.out.println("\n" + query.split(" ")[1]);
						
			switch(SelectQueryTypes.getQueryCategory(query)){
				case QueryConstants.MAX_PRICE_QUERY:
					System.out.println(ParsedData.getMaxPrice());
					break;
					
				case QueryConstants.SELECT_BRAND_FROM_PRICE:
					String price = query.substring(query.indexOf(">") + 1, query.indexOf(";"));
					price = price.trim();
					System.out.println(ParsedData.getBrandFromPrice(Double.parseDouble(price)));
					break;
					
				case QueryConstants.UNIQ_STORE_QUERY:
					boolean stock = Boolean.parseBoolean(query.substring(query.indexOf("=") + 1, query.indexOf(";")));
					System.out.println(ParsedData.getStoresForStock(stock));
					break;
					
				case QueryConstants.STOCK_TITLE_BRAND:
					stock = Boolean.parseBoolean(query.substring(query.indexOf("=") + 1, query.indexOf("and")).trim());
					int brand = Integer.parseInt(query.substring(query.lastIndexOf("=") + 1, query.indexOf(";")).trim());
					System.out.println(ParsedData.getTitleBrandFromStock(stock, brand, -1));
					break;
					
				case QueryConstants.STOCK_TITLE_BRAND_STORE:
					stock = Boolean.parseBoolean(query.substring(query.indexOf("=") + 1, query.indexOf("and")).trim());
					brand = Integer.parseInt(query.substring(query.indexOf("brand=") + 6, query.indexOf("or")).trim());
					int store = Integer.parseInt(query.substring(query.lastIndexOf("=") + 1, query.lastIndexOf(" ")).trim());
					System.out.println(ParsedData.getTitleBrandFromStock(stock, brand, store));
					break;
			}
		}else{
			throw new QueryException(ExceptionMessages.UNRECOGNIZED_SELECT_QUERY);
		}
	}

}
