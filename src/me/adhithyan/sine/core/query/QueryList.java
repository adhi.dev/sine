package me.adhithyan.sine.core.query;

import java.util.LinkedList;
import java.util.TreeMap;

/**
 *
 * @author adhithyan-3592
 *
 */

public class QueryList {

	private static LinkedList<String> availableQueries;
	private static TreeMap<String, String> queryInfo;
	
	static{
		availableQueries = new LinkedList<String>();
		queryInfo = new TreeMap<String, String>();
		
		availableQueries.add("load");
		queryInfo.put("load <file name> ", "- to load a csv file for querying.");

		availableQueries.add("reload");
		availableQueries.add("reload;");
		//queryInfo.put("reload ", "- to reload previously loaded csv file.");
		
		availableQueries.add("show");
		availableQueries.add("show;");
		queryInfo.put("show ", "- to display the contents of loaded csv file.");
		
		availableQueries.add("help");
		availableQueries.add("help;");
		queryInfo.put("help ", "- to view list of available queries or to know how to use sine.");
		
		availableQueries.add("select");
		queryInfo.put("select ", "- to view the data according to where condition and select column(s) .");
	}
	
	public static boolean supportedQueryCheck(String query){
		if(availableQueries.contains(query.split(" ")[0])){
			return true;
		}
		
		return false;
	}
	
	public static TreeMap<String, String> getAvailableQueriesInfo(){
		return queryInfo;
	}
}
