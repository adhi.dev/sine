package me.adhithyan.sine.core.query;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/**
 *
 * @author adhithyan-3592
 *
 */

public class SelectQueryTypes {

	private static HashSet<String> queries = new HashSet<String>();
	private static HashMap<String, String> queryCategoryMap = new HashMap<String, String>();
	
	static{
		queries.add("SELECT MAX(price) FROM products;".toLowerCase());
		queryCategoryMap.put("SELECT MAX(price) FROM products;".toLowerCase(), QueryConstants.MAX_PRICE_QUERY);
		
		queries.add("SELECT brand FROM products WHERE price >".toLowerCase());
		queryCategoryMap.put("SELECT brand FROM products WHERE price >".toLowerCase(), QueryConstants.SELECT_BRAND_FROM_PRICE);
		
		queries.add("SELECT UNIQ(store) FROM products WHERE in_stock=".toLowerCase());
		queryCategoryMap.put("SELECT UNIQ(store) FROM products WHERE in_stock=".toLowerCase(), QueryConstants.UNIQ_STORE_QUERY);
		
	}
	
	public static boolean isValidSelectQuery(String selectQuery){
		Iterator<String> it = queries.iterator();
		
		while(it.hasNext()){
			String query = it.next();
			
			String temp = "";
			if(query.contains(">")){
				temp = trimBrandSelectQuery(selectQuery);	
			}
			
			if(query.contains("uniq")){
				temp = trimUniqSelectQuery(selectQuery);
			}
			
			if(query.contains("max")){
				temp = query;
			}
			
			if(query.equals(temp)){
				return true;
			}else{
				if(query.contains("in_stock") && query.contains("brand") || query.contains("store")){
					return true;
				}
			}
			
		}
		
		return false;
	}
	
	public static String getQueryCategory(String query){
		if(query.contains(">")){
			query = trimBrandSelectQuery(query);
		}
		
		if(query.contains("uniq")){
			query = trimUniqSelectQuery(query);
		}
		
		if(query.contains("in_stock")){
			if(query.contains("title") && query.contains("brand") && !query.contains("store")){
				return QueryConstants.STOCK_TITLE_BRAND;
			}
			
			if(query.contains("title") && query.contains("brand") && query.contains("store")){
				return QueryConstants.STOCK_TITLE_BRAND_STORE;
			}
		}
		
		return queryCategoryMap.get(query);
	}
	
	private static String trimBrandSelectQuery(String query){
		if(query.contains(">")){
			return query.substring(0, query.indexOf(">") + 1);
		}
		
		return "";
	}
	
	private static String trimUniqSelectQuery(String query){
		if(query.contains("=")){
			return query.substring(0, query.indexOf("=") + 1);
		}
		
		return "";
	}
}
