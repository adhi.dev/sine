package me.adhithyan.sine.core.query;

import me.adhithyan.csv.parser.CsvLoader;
import me.adhithyan.sine.core.exceptions.ExceptionMessages;
import me.adhithyan.sine.core.exceptions.QueryException;

/**
 *
 * @author adhithyan-3592
 *
 */

public class LoadQueryImpl implements Query {

	@Override
	public void execute(String query) throws Exception{
		String[] parsedLoadQuery = query.split(" ");
		
		if(parsedLoadQuery.length == 1){
			throw new QueryException(ExceptionMessages.CSV_FILE_NOT_PROVIDED);
		}else{
			
			String fileName = parsedLoadQuery[1].substring(0, parsedLoadQuery[1].length()-1);
			if(!fileName.contains(".csv")){
				throw new QueryException(ExceptionMessages.NO_CSV_EXTENSION);
			}else{
				CsvLoader.loadCsv(fileName);
				System.out.println(fileName + " is loaded to current session.");
			}
		}
	}

}
