package me.adhithyan.sine.core;

import me.adhithyan.sine.core.exceptions.ExceptionMessages;
import me.adhithyan.sine.core.exceptions.QueryException;
import me.adhithyan.sine.core.query.QueryList;

/**
*
* @author adhithyan-3592
*
*/

class QueryValidator {

	public static void validateQuery(String query) throws Exception{
		int length = query.length();
		
			if(!QueryList.supportedQueryCheck(query)){
				throw new QueryException(ExceptionMessages.UNSUPPORTED_QUERY);
			}
			
			if(query.charAt(length - 1) != ';'){
				throw new QueryException(ExceptionMessages.SEMICOLON_MISSING);
			}
	}
}
