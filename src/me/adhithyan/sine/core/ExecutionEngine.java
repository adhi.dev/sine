package me.adhithyan.sine.core;

/**
*
* @author adhithyan-3592
*
*/

public class ExecutionEngine {

	public static void execute(String query){
		try{
			QueryValidator.validateQuery(query);
			
			QueryParser.parse(query);
			
			
			QueryExecutor.execute(query);
		}catch(Exception ex){
			System.out.println(ex.getMessage());
		}
	}
}
