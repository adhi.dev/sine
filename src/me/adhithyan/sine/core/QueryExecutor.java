package me.adhithyan.sine.core;

import me.adhithyan.sine.core.query.HelpQueryImpl;
import me.adhithyan.sine.core.query.LoadQueryImpl;
import me.adhithyan.sine.core.query.Query;
import me.adhithyan.sine.core.query.QueryType;
import me.adhithyan.sine.core.query.ReloadQueryImpl;
import me.adhithyan.sine.core.query.SelectQueryImpl;
import me.adhithyan.sine.core.query.ShowQueryImpl;

/**
*
* @author adhithyan-3592
*
*/

class QueryExecutor {

	public static void execute(String query) throws Exception{
		
		switch(QueryType.getQueryType()){
			
		case "load":
			Query loadQuery = new LoadQueryImpl();
			loadQuery.execute(query);
			break;
			
		case "show;":
			Query showQuery = new ShowQueryImpl();
			showQuery.execute(query);
			break;
			
		case "help;":
			Query helpQuery = new HelpQueryImpl();
			helpQuery.execute(query);
			break;
			
		case "select":
			Query selectQuery = new SelectQueryImpl();
			selectQuery.execute(query);
			break;
		}
	}
}
