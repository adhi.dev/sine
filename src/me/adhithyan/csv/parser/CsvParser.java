package me.adhithyan.csv.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

/**
*
* @author adhithyan-3592
*
*/

public class CsvParser {

	File file;
	String fileName;
	int count = 0;
	
	public CsvParser(File file){
		this.file = file;
	}
	
	public CsvParser(String fileName){
		this.fileName = fileName;
		file = new File(fileName);
	}
	
	public String parse(){
		BufferedReader br = null;
		//int count = 0;
		
		try {
			 br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
			 
			 StringBuffer content = new StringBuffer();
			 String line = "";
			 
			 while((line = br.readLine()) != null){
				 addToParsedData(line);
				 content.append(line + "\n");
			 }
			 
			
			return content.toString();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} finally{
			if(br != null){
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	public void addToParsedData(String line){
		String[] splittedData = line.split(",");
		
		count++;
		
		if(count > 1){
			String title = splittedData[0];
			int brand = Integer.parseInt(splittedData[1]);
			int store = Integer.parseInt(splittedData[2]);
			double price = Double.parseDouble(splittedData[3]);
			boolean stock = Boolean.parseBoolean(splittedData[4]);
					
			ParsedData.setMaxPrice(price);
			ParsedData.mapPriceToBrand(price, brand);
			ParsedData.mapStockAvailabitiltyInStore(stock, store);
			ParsedData.addStockTitleBrandMapping(stock, brand, title);
			ParsedData.addStoreTitlesMapping(store, title);
		}
	}
	
}
