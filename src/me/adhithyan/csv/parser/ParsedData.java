package me.adhithyan.csv.parser;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;

/**
 *
 * @author adhithyan-3592
 *
 */

public class ParsedData {

	//titleDataMap
	//brandDataMap
	//storeDataMap
	//priceDataMap
	//stockDataMap
	public static HashMap<Double, StringBuilder> brandPriceMapping = new HashMap<Double, StringBuilder>();
	public static HashMap<Boolean, TreeSet<Integer>> stockStoreMapping = new HashMap<Boolean, TreeSet<Integer>>(); 
	public static HashMap<Boolean, HashMap<Integer, StringBuilder>> stockTitleBrandMapping = new HashMap<Boolean, HashMap<Integer, StringBuilder>>();
	public static HashMap<Integer, StringBuilder> storeTitlesMapping = new HashMap<Integer, StringBuilder>();
	public static double maxPrice = 0d;
	
	
	public static void setMaxPrice(double currentPrice){
		maxPrice = (currentPrice >= maxPrice) ? currentPrice : maxPrice;

	}
	
	
	public static double getMaxPrice(){
		return maxPrice;
	}
	
	public static void mapPriceToBrand(double price, int brand){
		StringBuilder brands;
		
		if(brandPriceMapping.containsKey(price)){
			brands = brandPriceMapping.get(price);
		}else{
			brands = new StringBuilder();
		}
		
		brands.append(brand);
		brands.append("\n");
		brandPriceMapping.put(price, brands);
	}
	
	public static String getBrandFromPrice(double price){
		Iterator<Double> it = brandPriceMapping.keySet().iterator();
		StringBuilder output = new StringBuilder();
		
		while(it.hasNext()){
			Double currentPrice = it.next();
			StringBuilder currentBrands = brandPriceMapping.get(currentPrice);
			if(currentPrice > price){
				output.append(currentBrands);
			}
		}
		
		return output.toString();
	}
	
	public static void mapStockAvailabitiltyInStore(boolean stock, int store){
		TreeSet<Integer> stores;
		
		if(stockStoreMapping.containsKey(stock)){
			stores = stockStoreMapping.get(stock);
		}else{
			stores = new TreeSet<Integer>();
		}
		
		stores.add(store);
		stockStoreMapping.put(stock, stores);
	}
	
	public static String getStoresForStock(boolean stock){
		TreeSet<Integer> stores = stockStoreMapping.get(stock);
		
		Iterator<Integer> it = stores.iterator();
		
		StringBuilder output = new StringBuilder();
		
		while(it.hasNext()){
			output.append(it.next());
			output.append("\n");
		}
		
		return output.toString().trim();
	}
	
	public static void addStockTitleBrandMapping(boolean stock, int brand, String title){
		HashMap<Integer, StringBuilder> brandTitleMapping;
		StringBuilder sb = new StringBuilder();
		
		if(stockTitleBrandMapping.containsKey(stock)){
			brandTitleMapping = stockTitleBrandMapping.get(stock);
			
			if(brandTitleMapping == null){
				brandTitleMapping = new HashMap<Integer, StringBuilder>();
				sb = new StringBuilder();
			}else{
				if(brandTitleMapping.containsKey(brand)){
					sb = brandTitleMapping.get(brand);
				}else{
					sb = new StringBuilder();
				}
			}
			
		}else{
			brandTitleMapping = new HashMap<Integer, StringBuilder>();
			sb = new StringBuilder();
		}
	
		sb.append(title);
		sb.append("\n");
		brandTitleMapping.put(brand, sb);
		
		stockTitleBrandMapping.put(stock, brandTitleMapping);
		
	}
	
	public static String getTitleBrandFromStock(boolean stock, int inputBrand, int store){
		HashMap<Integer, StringBuilder> brandTitleMapping = stockTitleBrandMapping.get(stock);
		boolean storeRequired = (store == -1) ? false : true;
		
		StringBuilder output = new StringBuilder();
		if(brandTitleMapping != null){
			Iterator<Integer> it = brandTitleMapping.keySet().iterator();
			
			while(it.hasNext()){
				int brand = it.next();
				if(inputBrand == brand){
					output.append(brandTitleMapping.get(brand));
				}
			}
					
		}
		
		if(storeRequired){
			output.append(storeTitlesMapping.get(store));
		}
		
		return output.toString().trim();
	}
	
	public static void clear(){
		 brandPriceMapping.clear();
		 stockStoreMapping.clear(); 
		 maxPrice = 0d;
	}
	
	public static void addStoreTitlesMapping(int store, String title){
		StringBuilder sb;
		
		if(storeTitlesMapping.containsKey(store)){
			sb = storeTitlesMapping.get(store);
		}else{
			sb = new StringBuilder();
		}
		
		sb.append(title);
		sb.append("\n");
		
		storeTitlesMapping.put(store, sb);
	}
	
}
