package me.adhithyan.csv.parser;

/**
 *
 * @author adhithyan-3592
 *
 */

public class CsvLoader {

	private static boolean loaded = false;
	private static CsvParser csvParser;
	private static String content = null;
	private static String fileName = null;
	
	public static void loadCsv(String csvFile){
		csvParser = new CsvParser(csvFile);
		content = csvParser.parse();
		
		loaded = true;
		fileName = csvFile;
	}
	
	public static boolean fileLoaded(){
		return loaded;
	}
	
	public static String getLoadedData(){
		return content;
	}
	
	public static void reload(){
		loadCsv(fileName);
		ParsedData.clear();
		
		System.out.println(fileName + " is reloaded to current session.");
	}
}
